
Install
=======

Make sure [node](https://nodejs.org/en/) minimum version 8.3 is installed, then
install **mdncomp** globally:

    npm i -g mdncomp

At install the latest data will be downloaded automatically.
 
Update the Browser Compatibility Data itself occasionally using the option:

    mdncomp --update

In version 2 you *can* use NPM to update the data, however, do note that this 
will only work if there is a new version of **mdncomp** itself even if there 
are new data available.

Linux/Darwin will likely have to update via "--update" in any case due to 
permission restrictions.


Using mdncomp
=============

- [See information for all options](./Options.md) (v2)
- [See information for how to use](./Using.md) (v2)
